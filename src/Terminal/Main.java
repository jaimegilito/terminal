package Terminal;

import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {

        comandos c = new comandos();

        boolean close = false;
        String texto = "";
        int num = 0;
        
        while (!close) {
            String tp = "";
            Scanner teclado = new Scanner(System.in);
            tp = teclado.nextLine();
            
            String []vector = tp.split(" ");

            switch (vector[0]) {
                case "help":
                    c.help();
                    System.out.println();
                    break;

                case "cd":
                    c.cd(vector);
                    System.out.println();
                    break;
                case "mkdir":
                    c.mkdir(vector);
                    System.out.println();
                    break;

                case "info":
                    c.info(vector);
                    System.out.println();
                    break;

                case "cat":
                    c.cat(vector);
                    System.out.println();
                    break;

                case "top":
                    c.top(vector);
                    System.out.println();
                    break;

                case "mkfile":
                    c.mkfile(vector, texto);
                    System.out.println();
                    break;

                case "write":
                    c.escribir(vector);
                    System.out.println();
                    break;

                case "dir":
                    c.dir(vector);
                    System.out.println();
                    break;

                case "delete":
                    c.delete(vector);
                    System.out.println();
                    break;

                case "clear":
                    c.clear();
                    System.out.println();
                    break;

                case "close":
                    close = true;
                    System.out.println();
                    break;

                default:
                    System.err.println("Comando no existente, consulta el comando help");
                    System.out.println();
            }
        }
    }
}
