package Terminal;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class comandos {
    java.nio.file.Path ruta = Paths.get(System.getProperty("user.dir"));

// HELP
    public void help() {
        System.out.println(" help -> Lista los comandos con una breve definición de lo que hacen.\n\n"
                + " cd -> Muestra el directorio actual.\n\n"
                + " mkdir (nombre_directorio) -> Crea un directorio en la ruta actual.\n\n"
                + " info (nombre) -> Muestra la información del elemento. Indicando FileSystem,\n"
                + "Parent, Root, Nº of elements, FreeSpace, TotalSpace y UsableSpace.\n\n"
                + " cat (nombreFichero) -> Muestra el contenido de un fichero.\n\n"
                + " top (numeroLineas) (nombreFichero) -> Muestra las líneas especificadas de\n"
                + "un fichero.\n\n"
                + " mkfile (nombreFichero) (texto) -> Crea un fichero con ese nombre y el\n"
                + "contenido de texto.\n\n"
                + " write (nombreFichero) (texto) -> Añade 'texto' al final del fichero\n"
                + "especificado.\n\n"
                + " dir -> Lista los archivos o directorios de la ruta actual.\n\n"
                + " delete (nombre) -> Borra el fichero, si es un directorio borra todo su\n"
                + "contenido y a si mismo.\n\n"
                + " close -> Cierra el programa.\n\n"
                + " Clear -> vacía la vista.");
    }

//CLEAR
    public void clear() {
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
    }

//CD
    public void cd(String[] valor) {
        Path ruta2 = ruta;
        if (valor.length == 1) {
            ruta2 = ruta;
            System.out.println(ruta2);
        } else if (valor.length == 2) {
            if (valor[1].equals("..")) {
                ruta2 = ruta.getParent();
                System.out.println(ruta2);
                ruta = ruta2;
            } else {
                String txt = valor[1];
                if (txt.contains("\\") || txt.contains("/")) {
                    ruta2 = Paths.get(txt);
                } else {
                    File directorio = new File(ruta.toString());
                    if (directorio.exists()) {
                        ruta2 = ruta.resolve(txt);
                        System.out.println(ruta2);
                        ruta = ruta2;
                    }
                }
            }
        }
    }

//MKDIR
    public void mkdir(String[] valor) {
        if (valor.length == 2) {
            if (valor[0].equals("mkdir")) {
                ruta = ruta.resolve(valor[1]);
                File directorio = new File(ruta.toString());
                if (directorio.exists()) {
                    System.out.println("Directorio existente");
                } else {
                    try {
                        directorio.mkdir();
                        System.out.println("Creado con éxito");
                    } catch (SecurityException ex) {
                        System.err.println("No tiene permisos");
                    }
                }
            } else {
                System.err.println("Error, puede usar el comando help");
            }
        }
    }

//INFO
    public void info(String[] valor) {
        if (valor.length == 2) {
            ruta = ruta.resolve(valor[1]);
            File directorio = new File(ruta.toString());
            if (directorio.exists()) {
                System.out.println(ruta.getFileName()
                        + "\n" + ruta.getFileSystem()
                        + "\n" + ruta.getParent()
                        + "\n" + ruta.getRoot()
                        + "\n" + ruta.getNameCount()
                        + "\n" + directorio.getFreeSpace()
                        + "\n" + directorio.getTotalSpace()
                        + "\n" + directorio.getUsableSpace());
            } else {
                System.out.println("El directorio no existe");
            }
        }
    }

//CAT
    public void cat(String[] valor) throws IOException, FileNotFoundException {
        if (valor.length == 2) {
            ruta = ruta.resolve(valor[1]);
            File directorio = new File(ruta.toString());
            if (directorio.exists() && directorio.isFile()) {
                FileReader fr = new FileReader(directorio);
                BufferedReader br = new BufferedReader(fr);
                String cadena = "";
                while (cadena != null) {
                    cadena = br.readLine();
                    if (cadena != null) {
                        System.out.println(cadena);
                    }
                }
                br.close();
            } else {
                System.err.println("No se encuentra el fichero");
            }
        }
    }

//TOP
    public void top(String[] valor) throws FileNotFoundException, IOException {
        if (valor.length == 3) {
            ruta = ruta.resolve(valor[1]);
            File directorio = new File(ruta.toString());
            int numero = Integer.parseInt(valor[2]);
            if (directorio.exists() && directorio.isFile()) {
                FileReader fr = new FileReader(directorio);
                BufferedReader br = new BufferedReader(fr);
                String linea = "";
                int contador = 0;
                while (linea != null && contador < numero) {
                    linea = br.readLine();
                    contador++;
                    System.out.println(linea);
                }          
            }
        }
    }

//MKFILE
    public void mkfile(String[] valor, String texto) {
        try {
            if (valor.length == 3) {
                ruta = ruta.resolve(valor[1]);
                File fichero = new File(ruta.toString());
                if (fichero.createNewFile()) {
                    System.out.println("El fichero se ha creado correctamente");
                    try {
                        FileWriter archivo = new FileWriter(fichero);
                        BufferedWriter bw = new BufferedWriter(archivo);
                        PrintWriter salida = new PrintWriter(bw);
                        texto = valor[2];
                        salida.print(texto);
                        System.out.println("El fichero " + fichero + " se ha creado, y has escrito " + texto + "\n");
                        salida.close();
                        bw.close();
                    } catch (IOException ex) {
                        System.err.println(ex.getMessage());
                    }
                } else {
                    System.out.println("No ha podido ser creado el fichero, ya existe");
                }
            }
        } catch (IOException ex) {
            System.err.println("Ruta no válida");
        }
    }

//WRITE
    public void escribir(String[] valor) throws IOException {
        Scanner wr = new Scanner(System.in);
        if (valor.length == 2) {
            ruta = ruta.resolve(valor[1]);
            File fichero = new File(ruta.toString());
            if (fichero.exists()) {
                try {
                    FileWriter archivo = new FileWriter(fichero, true);
                    BufferedWriter bw = new BufferedWriter(archivo);
                    String frase;
                    System.out.println("Introduce lo que quieres escribir: ");
                    frase = wr.nextLine() + "\n";
                    for (int i = 0; i < frase.length(); i++) {
                        bw.write(frase.charAt(i));
                    }
                    bw.flush();
                    archivo.close();
                    bw.close();
                } catch (IOException ex) {
                    System.err.println("ERROR: al escribir");
                }
            } else {
                System.err.println("No se encuentra el fichero");
            }
        }
        System.out.println("Se ha escrito correctamente");
    }
    
//DIR
    public void dir(String[] valor) {
        if (valor.length == 1) {
            File directorio = new File(ruta.toString());
            if (directorio.exists()) {
                String[] ficheros = directorio.list();
                if (ficheros == null) {
                    System.out.println("No hay ficheros en el directorio");
                } else {
                    for (int x = 0; x < ficheros.length; x++) {
                        System.out.println(ficheros[x]);
                    }
                }
            } else {
                System.err.println("El directorio no existe");
            }
        } else if (valor.length == 2) {
            if (valor[1].equals(":\\") || valor[1].equals("/")) {
                File archivo = new File(valor[1]);
                if (archivo.exists()) {
                    File[] lista = archivo.listFiles();
                    for (int i = 0; i < lista.length; i++) {
                        System.out.println(lista[i].getName());
                    }
                } else {
                    System.err.println("La ruta no existe");
                }
            } else {
                File directorio = new File(ruta.resolve(valor[1]).toString());
                if (directorio.exists() && directorio.isDirectory()) {
                    File[] lista = directorio.listFiles();
                    if (lista.length == 0) {
                        System.err.println("El directorio vacío");
                    } else {
                        for (int i = 0; i < lista.length; i++) {
                            System.out.println(lista[i].getName());
                        }
                    }
                } else {
                    System.out.println("No se encuentra el directorio");
                }
            }
        }
    }

//DELETE
    public void borrarDirectorio(File directorio) {
        File[] archivos = directorio.listFiles();
        for (int i = 0; i < archivos.length; i++) {
            if (archivos[i] == directorio) {
                borrarDirectorio(archivos[i]);
            }
            archivos[i].delete();
        }
    }
    public void delete(String[] valor) {
        if (valor.length == 2) {
            ruta = ruta.resolve(valor[1]);
            File directorio = new File(ruta.toString());
            if (directorio.exists()) {
                borrarDirectorio(directorio);
                directorio.delete();
            } else {
                System.out.println("No se encuentra el fichero");
            }
        } else {
            System.out.println("Ruta no encontrada");
        }
    }
}
